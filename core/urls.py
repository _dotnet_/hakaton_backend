from django.contrib import admin
from django.urls import path, include
from core import views

urlpatterns = [
    path('category/', views.CategoryList.as_view()),
    path('category/<int:pk>', views.CategoryDetail.as_view()),
    path('place/', views.PlaceList.as_view()),
    path('place/<int:pk>', views.PlaceDetail.as_view()),
    path('event/', views.EventList.as_view()),
    path('event/<int:pk>', views.EventDetail.as_view()),
    path('recommendations/', views.RecomendationEventList.as_view()),
    path('set_liked_categories/', views.set_liked_categories),
    path('set_favorite_event/', views.set_favorite_event),
    path('del_favorite_event/', views.del_favorite_event),
]