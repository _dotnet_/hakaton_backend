from djoser.serializers import UserCreateSerializer, UserSerializer
from django.contrib.auth import get_user_model

from core.serializers import CategorySerializer, EventSerializer

User = get_user_model()


class UserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ("id", "email", "username", "first_name", "last_name", "password")

class CustomUserSerializer(UserSerializer):
    liked_categories = CategorySerializer(many=True)
    favorites = EventSerializer(many=True)
    class Meta(UserSerializer.Meta):
        model = User
        fields = ("id", "email", "username", "first_name", "last_name", 'liked_categories', 'favorites')
