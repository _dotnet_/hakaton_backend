from django.contrib import admin
from core.models import Category, Place, Event

# Register your models here.
admin.site.register(Category)
admin.site.register(Place)
admin.site.register(Event)