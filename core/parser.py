from bs4 import BeautifulSoup
import requests
import unicodedata

request = requests.get("https://hanty-mansiysk.info/afisha")
soup = BeautifulSoup(request.content, features="html.parser")
articles_block = soup.find_all("div", {"class": "col-md-9"})

info = []

for item in articles_block:
    el = (
       {"tittle": item.h2.a.text, 
        "date": item.p.strong.text.replace(u'\xa0', u' '), 
        "category": item.p.find_all("span")[0].strong.a.text, 
        "place": item.p.find_all("span")[1].strong.text}        
    )
    info.append(el)
    print(el)
