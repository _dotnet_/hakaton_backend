from rest_framework import serializers
from core.models import Category, Place, Event




class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ['id',
                  'name',
                  'latitude',
                  'longitude',
                  'description',
                  'start_time',
                  'end_time',
                  'photo',
                  ]


class EventSerializer(serializers.ModelSerializer):


    place = PlaceSerializer(required=True)
    category = CategorySerializer(required=True)

    class Meta:
        model = Event
        fields = ['id',
                  'name',
                  'description',
                  'date_start',
                  'minimum_age',
                  'price',
                  'photo',
                  'place',
                  'category',
                  'organizator'
                  ]

    def create(self, validated_data):
        place_data = validated_data.pop("place")
        place = Place.objects.get(name=place_data.get("name"))
        category_data = validated_data.pop("category")
        category = Category.objects.get(name=category_data.get("name"))

        instance = Event.objects.create(place=place, category=category, **validated_data)

        return instance