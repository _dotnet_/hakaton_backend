from django.shortcuts import render
from rest_framework import generics
from core.serializers import CategorySerializer, PlaceSerializer, EventSerializer
from core.models import Category, Place, Event
from rest_framework import permissions, viewsets
from account.models import CustomUser
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response



from django.db.models import Q

from django.views.decorators.csrf import csrf_exempt

from django.http import JsonResponse


# Create your views here.

class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class PlaceList(generics.ListCreateAPIView):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class PlaceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class EventList(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class RecomendationEventList(generics.ListAPIView):
    serializer_class = EventSerializer

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        condition = Q()

        user = self.request.user

        liked_categories = CustomUser.objects.get(pk=user.id).liked_categories.all()

        for cat in liked_categories:
            print(cat.name)

            condition |= Q(category__name=cat.name)
        queryset = Event.objects.filter(condition).order_by('-date_start')
        return queryset


@api_view(['POST'])
def set_liked_categories(request):
    if request.method == 'POST':
        try:
            user_id = int(request.data['user_id'])
            liked_categories_id = request.data['liked_categories']

            user = CustomUser.objects.get(pk=user_id)

            for cat_id in liked_categories_id:
                category = Category.objects.get(pk=int(cat_id))
                user.liked_categories.add(category)
            user.save()
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def set_favorite_event(request):
    if request.method == 'POST':
        try:
            user_id = int(request.data['user_id'])
            favorite_event_id = int(request.data['favorite_event_id'])

            user = CustomUser.objects.get(pk=user_id)
            event = Event.objects.get(pk=favorite_event_id)
            user.favorites.add(event)
            user.save()
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def del_favorite_event(request):
    if request.method == 'POST':
        try:
            user_id = int(request.data['user_id'])
            favorite_event_id = int(request.data['favorite_event_id'])

            user = CustomUser.objects.get(pk=user_id)
            event = Event.objects.get(pk=favorite_event_id)
            user.favorites.remove(event)
            user.save()
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

