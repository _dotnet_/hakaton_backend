from account.models import CustomUser
from django.db import models



class Category(models.Model):
    name = models.CharField(max_length=255)
    users = models.ManyToManyField(CustomUser, related_name="liked_categories", blank=True)

    def __str__(self):
        return self.name


class Place(models.Model):
    name = models.CharField(max_length=255)
    latitude = models.DecimalField(decimal_places=2, max_digits=6)
    longitude = models.DecimalField(decimal_places=2, max_digits=6)
    description = models.CharField(max_length=255)
    start_time = models.TimeField()
    end_time = models.TimeField()
    photo = models.FileField(upload_to ='photos/place_photos/', default='photos/place_photos/unknow_place.jpg')

    def __str__(self):
        return self.name

class Event(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=511)
    date_start = models.DateTimeField()
    minimum_age = models.PositiveIntegerField(default=0)
    price = models.DecimalField(decimal_places=2, max_digits=7)
    photo = models.FileField(upload_to ='photos/event_photos/', default='photos/event_photos/unknow_event.jpg')
    place = models.ForeignKey(Place, on_delete=models.SET_NULL, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    organizator = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    users = models.ManyToManyField(CustomUser, related_name="favorites", blank=True)

    def __str__(self):
        return self.name
